package com.atguigu.gulimall.ware.dao;

import com.atguigu.gulimall.ware.entity.UndoLogEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 
 * 
 * @author zbb
 * @email 695088491@qq.com
 * @date 2024-03-18 22:18:40
 */
@Mapper
public interface UndoLogDao extends BaseMapper<UndoLogEntity> {
	
}
