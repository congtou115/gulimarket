package com.atguigu.gulimall.member.dao;

import com.atguigu.gulimall.member.entity.MemberEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 会员
 * 
 * @author zbb
 * @email 695088491@qq.com
 * @date 2024-03-18 22:17:18
 */
@Mapper
public interface MemberDao extends BaseMapper<MemberEntity> {
	
}
