package com.atguigu.gulimall.coupon.dao;

import com.atguigu.gulimall.coupon.entity.CouponEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 优惠券信息
 * 
 * @author zbb
 * @email 695088491@qq.com
 * @date 2024-03-18 22:14:00
 */
@Mapper
public interface CouponDao extends BaseMapper<CouponEntity> {
	
}
