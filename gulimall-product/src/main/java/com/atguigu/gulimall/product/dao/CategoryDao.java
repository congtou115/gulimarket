package com.atguigu.gulimall.product.dao;

import com.atguigu.gulimall.product.entity.CategoryEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 商品三级分类
 * 
 * @author zbb
 * @email 695088491@qq.com
 * @date 2024-03-18 20:49:30
 */
@Mapper
public interface CategoryDao extends BaseMapper<CategoryEntity> {
	
}
